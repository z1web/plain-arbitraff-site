<?php
/**
 * zaimov.net renderer
 *
 * @author D.Kuschenko <kd@linkprofit.com>
 */

require_once __DIR__ . '/../autoload.php';

$app = new \src\Application('zaimov.net');

switch ($app->getRoute()) {
    case '/y' :
        $app->renderView('light');
        break;
    case '/u' :
        $app->renderView('gangbang');
        break;
    case '/' :
        $app->renderView('index', $app->offersApi->getOffers(66));
        break;
}
