<?php
/**
 * @var $data array
 */
$offers = $data['offers']
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zaimov.Net но есть много денег без процентов! </title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>

    <!-- Google Tag Manager -->
    <script>(function (w, d, s, l, i) {
            w[l] = w[l] || [];
            w[l].push({
                'gtm.start':
                    new Date().getTime(), event: 'gtm.js'
            });
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', 'GTM-WFXPJ7C');</script>
    <!-- End Google Tag Manager -->

    <link rel="shortcut icon" href="//arbitraff.ru/img/sites_favicons/66/favicon.ico"/>
    <!-- Best Offer Widget -->
    <script>
        bestOfferWidgetClosed = false;
        window.onscroll = function () {
            if (!bestOfferWidgetClosed)
                $('.best-offer-widget').slideDown();
        }

        function closeBest() {
            $('.best-offer-widget').slideUp();
            bestOfferWidgetClosed = true;
        }
    </script>
    <!-- End Best Offer Widget -->

    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFXPJ7C"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->


<script src="//wlink.ru/zaim/ga_link.js"></script>
<script>var gastr = new GaSrt('UA-119322183-4', '.offers a', 'data5', 'buy_ru');
    gastr.go();
    var gastrBestWidget = new GaSrt('UA-119322183-4', '.best-offer-widget a', 'data5', 'buy_ru');
    gastrBestWidget.go();

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-3101927-13"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());

    gtag('config', 'UA-3101927-13');
</script>

<!-- Best Offer Widget -->
<div style="display: none" class="best-offer-widget">
    <div class="best-offer-widget__wrap container">

        <div style="
    font-size: 14px;
">Получите спец.условия написав
        </div>
        <div class="social">
            <a href="tg://resolve?domain=money2ubot&start=ru_site" target="_blank" id="telegram"><img
                        src="https:///wlink.ru/zaim/img/tele.png" width="30"></a>
            &nbsp;
            <a href="https://vk.me/want.more.money2" target="_blank" id="vk_link"><img
                        src="https:///wlink.ru/zaim/img/vk.png" width="30"></a>
            &nbsp;
            <a href="https://m.me/591828671180227" target="_blank" id="fb_link"><img
                        src="https:///wlink.ru/zaim/img/fb.png" width="30"></a>
        </div>
        <div onclick="closeBest()" class="best-offer-widget__close">
            <div class="best-offer-widget__close-wrap"><span>
         <p class="glyphicon glyphicon-remove"></p></span></div>
        </div>
        <a target="_blank" href="http://pix15.link/scripts/click.php?ban_id=ks52dh67&aff_id=26383&data2=pl_best"
           id="banner">
            <div style="background-image: url('//arbitraff.ru/img/offers_logos/79.png')"
                 class="best-offer-widget__logo"></div>
            <div class="best-offer-widget__text"><span>Вам одобрен займ</span><br/><span>Заберете прямо сейчас?</span>
            </div>
        </a>


    </div>
</div>
<!-- End Best Offer Widget -->

<div class="wrap">
    <header class="header">
        <div class="header__topline">
            <div class="container">
                <a href="/" style="color:black">Zaimov.net</a>
                <ul class="header__nav">
                    <li><a href="/offers" class="header__nav-active">Предложения</a></li>
                    <!--                    <li><a href="/articles">Новости</a></li>-->
                    <!--                    <li><a href="/about">О нас</a></li>-->
                    <!--                    <li><a href="/contact">Контакты</a></li>-->
                </ul>
            </div>
        </div>
        <div class="header__bottomline">
            <div class="container">
                <h1 class="header__title">Лучшие предложения</h1>
            </div>
        </div>
    </header>
    <div class="container">
        <?php require_once 'offers-section.php'; ?>
    </div>
    <footer class="footer">
        <?php require_once 'footer-section.php'; ?>
    </footer>
</div>

<?php if (isset($_GET['style2'])) { ?>
    <link rel="stylesheet" href="/template/web/style2.css">
<?php } else { ?>
    <link rel="stylesheet" href="/template/web/style.css">
<?php } ?>

<script type="text/javascript" src="//arbitraff.ru/templates/topbankov/js/scripts.min.js"></script>

</html>