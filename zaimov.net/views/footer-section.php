<div class="container">


    <p>

    <h4 style="text-align: center;">Требования к заёмщику:<br></h4>
    <p class="MsoNormal"
       style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: normal;">
        - Паспорт гражданина РФ<br>- Возраст от 18 лет<br>- Кредитная история никак не влияет на одобрение заявок по
        займам<font color="#635d53" face="Helvetica, sans-serif"><span style="font-size: 11.5pt;"><o:p></o:p></span></font>
    </p>
    <p class="MsoNormal"
       style="margin-bottom: 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: normal;">
        <span style="font-weight: 700;"><span
                style="font-size: 11.5pt; font-family: Helvetica, sans-serif; color: rgb(99, 93, 83);">Условия выдачи кредита:</span></span><span
            style="font-size: 11.5pt; font-family: Helvetica, sans-serif; color: rgb(99, 93, 83);"><o:p></o:p></span>
    </p>
    <p class="MsoNormal"
       style="margin-bottom: 7.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: normal;">
        Минимальный процент по займу составляет 0% в год, а максимальный процент по займу 580% в год. Все кредиты
        выдаются на суммы от 1000 рублей и до максимальной суммы в &nbsp;3000000 рублей. Сроки представленных займов
        составляют от 61 дня. Максимальный срок займа составляет 5 лет. При выявлении просрочки выплат по займу,
        кредитная организация начинает начислять денежную неустойку, равную 0,1% от просроченной суммы ежедневно, но
        неустойка не может превышать 10% от общей суммы займа. При продолжительных задержках сроков выплат по займу
        информация о недобросовестном заемщике передается в БКИ (Бюро Кредитных Историй).<font color="#635d53"
                                                                                               face="Helvetica, sans-serif"><span
                style="font-size: 11.5pt;"><o:p></o:p></span></font></p>
    <p class="MsoNormal"
       style="margin-bottom: 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: normal;">
        <span style="font-weight: 700;"><span
                style="font-size: 11.5pt; font-family: Helvetica, sans-serif; color: rgb(99, 93, 83);">Пример расчета выплат по кредиту:<o:p></o:p></span></span>
    </p>
    <p class="MsoNormal"
       style="margin-bottom: 7.5pt; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: normal;">
        Заемщик взял кредит в размере 100 000 рублей на срок в 90 дней. Ежемесячная ставка по кредиту равна 10%. Сумма,
        которую будет должен вернуть заещик через 90 дней составляет &nbsp;130 000 рублей. В данную сумму входит 100 000
        рублей изначальной суммы займа, а также 30&nbsp;000 рублей составят проценты за использование кредитного
        продукта.<font color="#635d53" face="Helvetica, sans-serif"><span style="font-size: 11.5pt;"><o:p></o:p></span></font>
    </p>
    <p class="MsoNormal"
       style="margin-bottom: 7.5pt; text-align: justify; background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: normal;">
        <span style="font-weight: 700;"><span
                style="font-size: 11.5pt; font-family: Helvetica, sans-serif; color: rgb(99, 93, 83);">Пример расчета потребительского кредита:</span></span><span
            style="font-size: 11.5pt; font-family: Helvetica, sans-serif; color: rgb(99, 93, 83);"><o:p></o:p></span>
    </p>
    <p class="MsoNormal"
       style="background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; line-height: normal;">
        Заемщик берет кредит с процентной ставкой 16% годовых. Срок погашения кредита составляет 36 месяцев. Сумма
        кредита равна 500&nbsp;000 рублей. При данных условиях ежемесячная выплата по кредиту составит 20&nbsp;555
        рублей, сумма начисленных процентов за весь срок 240&nbsp;000 рублей, Итоговая же сумма выплат, включая
        проценты, будет равна 740&nbsp;000 рублей за 3 года (36 месяцев).</p><h4 style="text-align: center;">Последствия
        невыплаты заёмных средств:</h4>
    <p style="">

        В случае невозвращения в условленный срок суммы кредита или суммы процентов за пользование заёмными средствами
        кредитор вынуждено начислит штраф за просрочку платежа. Большинство кредиторов идут на уступки и дают 3
        дополнительных рабочих дня для оплаты.
        Они предусмотрены на случай, к примеру, если банковский перевод занял больше времени, чем обычно. Однако, в
        случае неполучения от Вас какой-либо реакции в течение продолжительного времени, будет начислен штраф за
        просрочку срока погашения размером в среднем 0,10% от первоначальной суммы для займов, 0,03% от суммы
        задолженности в среднем для потребительских кредитов и кредитных карт. При несоблюдении Вами условий по
        погашению кредитов и займов, данные о Вас могут быть переданы в реестр должников, а задолженность -
        коллекторскому агентству для взыскания долга.

        О всех приближающихся сроках платежа кредитор своевременно информирует Вас по СМС или электронной почте.
        Рекомендуем Вам вносить платеж в день получения данных напоминаний. Погашая задолженность в срок, Вы формируете
        хорошую кредитную историю, что повышает Ваши шансы в дальнейшем получить кредит на более выгодных условиях.
        Предложение не является оффертой. Конечные условия уточняйте при прямом общении с кредиторами.</p>
    <p>
    <hr style="text-align: center; box-sizing: content-box; margin-top: 10px; margin-bottom: 0px; border-top-color: rgb(177, 177, 177); font-family: Tahoma; font-size: 12px; background-color: rgb(246, 246, 246);">
    </p>

    <h4 style="text-align: center;">Информация о кредиторах, представленных на сайте:</h4>
    <p style="text-align: center;"><strong>АО «Тинькофф Банк»</strong></p>
    <p style="text-align: center;">№ лицензии : 2673</p>
    <p style="text-align: center;">Процентная ставка : 23,9 годовых</p>
    <p style="text-align: center;">Адрес: г.Москва, 1-й Волоколамский проезд, дом 10, стр.1; тел. 8 800 755-10-10</p>

    <p style="text-align: center;"><strong>ПАО «Совкомбанк»</strong></p>
    <p style="text-align: center;">№ лицензии : 963</p>
    <p style="text-align: center;">Процентная ставка : 12% годовых</p>
    <p style="text-align: center;">г. Кострома, пр. Текстильщиков, д. 46; тел. 8 800 200-66-96</p>
    <p style="text-align: center;"><strong>КБ "Ренессанс Кредит" (ООО)</strong></p>
    <p style="text-align: center;">№ лицензии : 3354</p>
    <p style="text-align: center;">Процентная ставка : 15,9% годовых</p>
    <p style="text-align: center;">г.Москва, ул.Кожевническая, дом 14; тел. 8 (495) 783-4600</p>
    <p style="text-align: center;"><strong>Банк ВТБ (ПАО)</strong></p>
    <p style="text-align: center;">№ лицензии : 2748</p>
    <p style="text-align: center;">Процентная ставка : 16,9% годовых</p>
    <p style="text-align: center;">г. Санкт-Петербург, ул. Большая Морская, д. 29; тел. 8 800 200-23-26</p>
    <p style="text-align: center;"><strong>ПАО КБ «УБРиР»</strong></p>
    <p style="text-align: center;">№ лицензии : 429&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 17% годовых&nbsp;</p>
    <p style="text-align: center;">г. Екатеринбург, ул. Сакко и Ванцетти, 67; тел. (343) 2-644-644</p>
    <p style="text-align: center;"><strong>ПАО "СКБ-банк"</strong></p>
    <p style="text-align: center;">№ лицензии : 705&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 15,9% годовых&nbsp;</p>
    <p style="text-align: center;">г. Екатеринбург, ул. Куйбышева, 75; тел. (343) 355-75-75</p>
    <p style="text-align: center;"><strong>АО "ОТП Банк"</strong></p>
    <p style="text-align: center;">№ лицензии : 2766&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 14,9% годовых&nbsp;</p>
    <p style="text-align: center;">г.Москва, Ленинградское шоссе, д.16А, стр.1; тел. 8 (495) 775-4-775</p>
    <p style="text-align: center;"><strong>АО «АЛЬФА-БАНК»</strong></p>
    <p style="text-align: center;">№ лицензии : 1326&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 26,99% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, ул. Каланчевская, 27; тел. 8 (495) 620-91-91</p>


    <p style="text-align: center;"><strong>Kredito24</strong></p>
    <p style="text-align: center;">№ лицензии : 651303552003006&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 365% годовых&nbsp;</p>
    <p style="text-align: center;">г.Москва, ул.Новослободская, дом 14/19, стр.1, офис 29, к. 5; тел. 8 (495)
        225-90-63</p>
    <p style="text-align: center;"><strong>MoneyMan</strong></p>
    <p style="text-align: center;">№ лицензии : 2110177000478&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 273,75% годовых&nbsp;</p>
    <p style="text-align: center;">г.Москва, ул. Барклая, д. 6, стр. 9, офис 1; тел 8 (800) 77-555-76</p>


    <p style="text-align: center;"><strong>E-Zaem</strong></p>
    <p style="text-align: center;">№ лицензии : 651303045003161&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 700,8% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, ул. Земляной Вал, д. 33, этаж 4, помещение XVI, антресоль 1, комната 1;
        тел 8 (800) 775-77-75</p>
    <p style="text-align: center;"><strong>Займер</strong></p>
    <p style="text-align: center;">№ лицензии : 651303532004088&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 248,2% годовых&nbsp;</p>
    <p style="text-align: center;">г. Кемерово, пр-т Советский, д. 2/7.; тел 8 (800) 7070-24-7</p>
    <p style="text-align: center;"><strong>Мигкредит</strong></p>
    <p style="text-align: center;">№ лицензии : 2110177000037&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 211,7% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, ул. Сущевский Вал, д. 5, стр. 3; тел 8 (800) 100-06-09</p>
    <p style="text-align: center;"><strong>Екапуста</strong></p>
    <p style="text-align: center;">№ лицензии : 2120754001243&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 620,5% годовых&nbsp;</p>
    <p style="text-align: center;">г. Новосибирск, ул. Гнесиных 10/1; тел 8 (495) 215-55-67</p>
    <p style="text-align: center;"><strong>Mili</strong></p>
    <p style="text-align: center;">№ лицензии : 2120177001299&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 365% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, ул. Тверская, д. 9, стр. 7; тел 8 (800) 700-55-44</p>
    <p style="text-align: center;"><strong>SmartCredit</strong></p>
    <p style="text-align: center;">№ лицензии : 651503045006429&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 584% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, Саввинская наб. ул. 15; тел 8 (495) 646-10-35</p>
    <p style="text-align: center;"><strong>Честное слово</strong></p>
    <p style="text-align: center;">№ лицензии : 651303045002916&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 730% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, ул. Полковая, д.3, стр.4; email info@4slovo.ru</p>
    <p style="text-align: center;"><strong>Вкармане</strong></p>
    <p style="text-align: center;">№ лицензии : 651403550005450&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 365% годовых&nbsp;</p>
    <p style="text-align: center;">г. Новосибирск, ул. Дмитрия Шамшурина д. 1, офис 1; тел 8 (800) 500-35-85</p>
    <p style="text-align: center;"><strong>Pay P.S.</strong></p>
    <p style="text-align: center;">№ лицензии : 2120177001838&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 730% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, 123001, ул. Спиридоновка д.27/24; тел 8 (499) 703-38-52</p>
    <p style="text-align: center;"><strong>Центрзаймов</strong></p>
    <p style="text-align: center;">№ лицензии : 2110177000192&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 365% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, Хорошевское шоссе 35 корпус 1; тел 8 (800) 775-25-45</p>
    <p style="text-align: center;"><strong>Займи Просто</strong></p>
    <p style="text-align: center;">№ лицензии : 651303045003776&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 730% годовых&nbsp;</p>
    <p style="text-align: center;">г. Москва, Старокаширское ш., 2, к. 2; тел 8 (499) 709-80-94</p>
    <p style="text-align: center;"><strong>Lime Займ</strong></p>
    <p style="text-align: center;">№ лицензии : 651503045006452&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 730% годовых&nbsp;</p>
    <p style="text-align: center;">г. Новосибирск, ул. Крылова, д.36, офис 219; тел 8 (800) 700-01-97</p>
    <p style="text-align: center;"><strong>CreditPlus</strong></p>
    <p style="text-align: center;">№ лицензии : 651503045006452&nbsp;</p>
    <p style="text-align: center;">Процентная ставка : 584% годовых&nbsp;</p>
    <p style="text-align: center;"><br>г. Москва, Алтуфьевское шоссе, д. 44; 8 (800) 500-49-00</p>
    <p class="row" style="font-family: Tahoma; font-size: 12px; background-color: rgb(246, 246, 246);">
    <div class="row" style="font-family: Tahoma; font-size: 12px; background-color: rgb(246, 246, 246);"></div>
    </p>


    </p>

    <div id="privacy-policy" role="dialog" tabindex="-1" style="display: none" class="fade modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
                    <h2>Privacy policy label</h2>
                </div>
                <div class="modal-body"></div>
            </div>
        </div>
    </div>


    <a data-toggle="modal" data-target="#privacy-policy"></a>


</div>