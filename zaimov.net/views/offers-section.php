<section class="offers">
    <?php
    $route = parse_url($_SERVER['REQUEST_URI'])['path'];

    /**
     * @param $link
     * @param $route
     * @return mixed
     */
    function buildLink($link, $route)
    {
        if ($route !== '/') {
            $link = $link . '&chan=' . $route;
        }

        return $link;
    }

    ?>
    <?php foreach ($offers['offers'] as $offer): ?>
        <div class="offer">
            <div class="offer__top">
                <div class="offer__logo-wrap">
                    <a href="<?= buildLink($offer['link'], $route) ?>"
                       target="_blank">
                        <img src="<?= $offer['logoPath'] ?>" class="img-responsive offer__logo">
                    </a>
                </div>
                <div class="offer__desc-wrap">
                    <div class="offer__limit-wrap offer__limit--right">
                        <p class="offer__option-title">Сумма</p>
                        <p class="offer__option-desc">до <?= $offer['maxcreditsum'] ?> руб.</p>
                    </div>
                    <p class="offer__percent ">от&nbsp<span><?= $offer['mincreditpercent'] ?>%</span></p>
                    <div class="offer__period-wrap">
                        <p class="offer__option-title">Срок</p>
                        <p class="offer__option-desc">от <?= $offer['mincreditterm'] ?>
                            до <?= $offer['maxcreditterm'] ?> дней</p>
                    </div>
                </div>
                <div class="offer__button-wrap">
                    <a id="Займер" href="<?= buildLink($offer['link'], $route) ?>"
                       target="_blank" class="offer__button">Заполнить заявку</a></div>
            </div>
            <div class="offer__bottom">
                <p class="offer__bottom--accent-title"><?= $offer['opt_history'] ?></p>
                <div class="offer__bottom--item">
                    <p class="offer__bottom-title">Преимущества:</p>
                    <div class="offer__bottom-desc"><?= $offer['opt_early'] ?></div>
                </div>
                <div class="offer__bottom--item">
                    <p class="offer__bottom-title">Требования:</p>
                    <div class="offer__bottom-desc">возраст от <?= $offer['minage'] ?> лет</div>
                </div>
                <div class="offer__bottom--item">
                    <p class="offer__bottom-title">Преимущества:</p>
                    <div class="offer__bottom-desc"><?= $offer['opt_prolong'] ?></div>
                </div>
                <div class="offer__bottom--more-wrap">
                    <div class="offer__bottom--item">
                        <a id="<?= $offer['creditorganizationname'] ?>"
                           href="<?= $offer['opt_prolong'] ?>"
                           target="_blank">
                            <div class="offer__bottom-desc"><?= $offer['descriptionfull'] ?></div>
                        </a>
                    </div>
                </div>
            </div>
            <?php if ($offer['descriptionfull']) : ?>
                <div class="offer__bottom--link-more">Подробнее</div>
            <?php endif; ?>

        </div>
    <?php endforeach; ?>

</section>
