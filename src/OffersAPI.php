<?php
/**
 * Клиент для работы с API Arbitraff
 *
 * @author D.Kuschenko [z[1]FFy]
 */
namespace src;

class OffersAPI
{
    protected $apiUrl = 'http://arbitraff.ru/api';
    protected $hash = 'hjsadSdsvBVdCsdsdsbvg';

    /**
     * @var FileCache
     */
    public $cache;

    /**
     * OffersAPI constructor.
     * @param $fileCache
     */
    public function __construct($fileCache)
    {
        $this->cache = $fileCache;
    }

    /**
     * Получение офферов
     *
     * @param $siteId
     * @return mixed
     */
    public function getOffers($siteId)
    {
        $cached = $this->cache->get($siteId);
        if ($cached) {
            return $cached;
        }

        $response = $this->sendCurl(
            $this->buildUrl('get-offers',
                [
                    'siteId' => $siteId
                ]
            )
        );

        if ($response['result'] === 'Success') {
            $this->cache->save($siteId, $response, 3600);
            return $response;
        }

        return false;
    }

    /**
     * @param $siteId
     * @return bool|mixed
     */
    public function getSiteData($siteId)
    {
        $cacheKey = 'siteData-'.$siteId;
        $cached = $this->cache->get($cacheKey);
        if ($cached) {
            return $cached;
        }

        $response = $this->sendCurl(
            $this->buildUrl('get-site-data', ['siteId' => $siteId])
        );

        if ($response['result'] === 'Success') {
            $this->cache->save($cacheKey, $response, 3600);
            return $response;
        }

        return null;
    }

    /**
     * @param $siteId
     * @return bool|mixed
     */
    public function getSiteMessages($siteId)
    {
        $cacheKey = 'siteMessages-'.$siteId;
        $cached = $this->cache->get($cacheKey);

        if ($cached) {
            return $cached;
        }

        $response = $this->sendCurl(
            $this->buildUrl('get-site-messages', ['siteId' => $siteId])
        );

        if ($response['result'] === 'Success') {
            $this->cache->save($cacheKey, $response, 3600);
            return $response;
        }

        return null;
    }

    /**
     * Отправка запроса
     *
     * @param $url
     * @return mixed
     */
    protected function sendCurl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);

        curl_close($ch);

        return json_decode($output, 1);
    }

    /**
     * Генерим адрес для отправки
     *
     * @param $method
     * @param $data
     * @return string
     */
    protected function buildUrl($method, $data)
    {
        $data['hash'] = $this->hash;
        $url = $this->apiUrl . '/' . $method . '?' . http_build_query($data);

        return $url;
    }
}