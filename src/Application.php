<?php

/**
 * @author D.Kuschenko <kd@linkprofit.com>
 */

namespace src;


class Application
{
    public

        /**
         * @var string
         */
        $route,
        $siteFolder,
        $siteTitleName,
        $siteId,

        /**
         * @var OffersAPI
         */
        $offersApi,

        /**
         * @var FileCache
         */
        $fileCache;

    /**
     * Application constructor.
     * @param $siteFolder
     * @param $siteTitleName
     * @param $siteId
     */
    public function __construct($siteFolder, $siteTitleName, $siteId)
    {
        $this->fileCache = new FileCache();
        $this->offersApi = new OffersAPI($this->fileCache);

        $this->siteFolder = $siteFolder;
        $this->siteTitleName = $siteTitleName;
        $this->siteId = $siteId;

        $this->initRoute();
        $this->errorReporting(true);
    }


    /**
     * @param $view
     * @param array $incomingData
     */
    public function renderView($view, $incomingData = [])
    {
        $data = $incomingData;
        $viewPath = __DIR__ . '/../' . $this->siteFolder . '/views/'.$view.'.php';
        if (file_exists($viewPath)) {
            require_once $viewPath;
        } else {
            $this->notFoundResponse();
        }
    }

    protected function cleanCacheCallback()
    {
        if ($this->route === '/clean-cache') {
            $this->offersApi->cache->clearAllCache();
            $this->stopApplication();
        }
    }

    /**
     * @param $bool boolean
     */
    protected function errorReporting($bool)
    {
        if ($bool) {
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }
    /**
     * Getting route string
     */
    protected function initRoute()
    {
        $requestUri = @$_SERVER['REQUEST_URI'];
        if ($requestUri) {
            $parsedUri = @parse_url($requestUri);
            if ($parsedUri) {
                $route = @$parsedUri['path'];
                if ($route) {
                    $this->route = $route;
                }
            }
        }

        $this->cleanCacheCallback();


        switch ($this->getRoute()) {
            case '/' :
                $offers = $this->offersApi->getOffers($this->siteId)['offers'];
                $data['title'] = $this->siteTitleName;
                $data['view'] = 'offers-section';
                $data['offers'] = $offers;

                $this->renderView('index', $data);
                break;
            case '/articles':
                $this->renderView('index', $this->articlesPage());
                break;
            case '/debet':
                $data['title'] = 'Дебетовые карты';
                $this->renderView('debet', $data);
                break;
            case '/credits':
                $data['title'] = 'Кредиты';
                $this->renderView('credits', $data);
                break;
            default:
                $this->proxyRoute();
                break;
        }


    }



    /**
     * @return mixed
     */
    public function articlesPage() {
        $data['articles'] = json_decode(file_get_contents('http://arbitraff.ru/api/get-site-articles?siteId='.$this->siteId.'&hash=hjsadSdsvBVdCsdsdsbvg'),1);

        if (isset($data['articles']['error'])) {
            $this->stopApplication();
        }

        if (isset($_GET['id'])) {
            foreach ($data['articles']['result_data'] as $article) {
                if ((int)$article['id'] === (int)$_GET['id']) {
                    $data['article'] = $article;
                    if (isset($article['seo_title'])) {
                        $data['title'] = @$article['seo_title'] ;
                    } else {
                        $data['title'] = @$article['name'];
                    }
                    $data['meta_description'] = @$article['seo_description'];
                    $data['meta_keywords'] = @$article['seo_keywords'];

                }
            }
            $data['view'] = 'oneArticle';
        } else {
            $data['title'] = 'Статьи ';
            $data['view'] = 'articles';
        }

        return $data;
    }

    public function proxyRoute()
    {
        $findStr = strpos($this->getRoute(), '/img/offers_logos');
        if ($findStr === 0) {
            $proxyData = file_get_contents('http://arbitraff.ru/'.$this->getRoute());

            $findExtStrPng = strpos($this->getRoute(), '.png');
            $findExtStrJpg = strpos($this->getRoute(), '.jpg');
            $ext = 'jpeg';

            if($findExtStrPng !== false) {
                $ext = 'png';
            }

            if($findExtStrJpg !== false) {
                $ext = 'jpeg';
            }

            $expires = 14 * 60*60*24;

            header("Content-Type: image/{$ext}");
            header("Content-Length: " . strlen($proxyData));
            header("Cache-Control: public", true);
            header("Pragma: public", true);
            header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT', true);

            echo $proxyData;
            exit;
        } else {
            $this->notFoundResponse();
        }
    }

    public static function buildLink($offer, $affId) {
        return 'https://x.generiq.ru/trace?aid='.$affId.'&ohash='.$offer['generiq_jab_offer_id'];
    }

    public static function buildLogoLink($link) {
        return str_replace('//arbitraff.ru/', '', $link);
    }


    /**
     * (:
     */
    protected function stopApplication()
    {
        die;
    }

    protected function notFoundResponse()
    {
        http_response_code(404);
        $this->stopApplication();
    }


}