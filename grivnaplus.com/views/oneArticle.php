<div class="container"><div>
    <h3 class="article__title"><?= $data['article']['name'] ?></h3>
    <div class="article__description">
        <?= $data['article']['text'] ?>
    </div>
    <small><?= $data['article']['pub_date'] ?></small>
</div>
</div>