<?php
/**
 * @var $data array
 */
$offers = $data['offers'];

?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=@$data['title']?></title>
    <meta name="keywords" content="<?=@$data['meta_keywords']?>" />
    <meta name="description" content="<?=@$data['meta_description']?>" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>

    <link rel="stylesheet" href="/web/style.css">

    <link rel="apple-touch-icon" sizes="180x180" href="/web/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/web/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/web/favicon-16x16.png">

    <?php
        $head = json_decode(file_get_contents('http://arbitraff.ru/api/get-site-data/?siteId=76&hash=hjsadSdsvBVdCsdsdsbvg'),1);
        echo @$head['content_in_head'];
    ?>
</head>

<body>


<section id="header">
    <div class="container container-fluid">
        <div class="row dblock">
            <!--<img src="img/odobrit-logo.png" alt="">-->
            <h2>GrivnaPlus.com - кредиты без отказа</h2>
        </div>
    </div>
</section>

<?php
require_once $data['view'].'.php';
?>
<?php require_once 'footer-section.php'; ?>

</body>
</html>