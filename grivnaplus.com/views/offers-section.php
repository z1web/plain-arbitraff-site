


<section id="cards">
    <div class="container">
    <div>
        <?php
        function buildLink($offer) {
            return 'https://generiq.go2cloud.org/aff_c?offer_id='.$offer['generiq_offer_id'].'&aff_id=1007';
        }

        function buildLogoLink($link) {
            return str_replace('//arbitraff.ru/', '', $link);
        }
        foreach ($offers as $offer): ?>


            <a style="margin: 10px 0" href="<?=buildLink($offer)?>" target="_blank" class="offer col-md-3 col-sm-6 col-xs-6">
                <div class="card card-item ">
                    <div class="mfo-logo">
                        <div class="offer-logo" style="height:120px;background-image: url('<?=buildLogoLink($offer['logoPath']);?>');background-repeat: no-repeat;
                            background-size: contain;
                            background-position: center;"></div>
                    </div>
                    <div class="term term3">
                        <p><?= $offer['opt_history'] ?></p>
                        <p><?= $offer['opt_early'] ?></p>
                    </div>
                    <div class="offer">
                        <div >


                        </div>
                        <div >
                            <b>Сумма</b>
                            <span>до <?= $offer['maxcreditsum'] ?> ₴</span>
                        </div>
                        <div >
                            <b>Процент</b>
                            <span>от <?= $offer['mincreditpercent'] ?>% в день</span>
                        </div>

                        <div >
                            <b>Срок</b>
                            <span>от <?= $offer['mincreditterm'] ?>
                                до <?= $offer['maxcreditterm'] ?> дней</span>
                        </div>
                    </div>
                    <div class="cta">
                        <button class="btn btn-get">Получить кредит</button>
                    </div>
                </div>
            </a>



        <?php endforeach; ?>

    </div>
    </div>
</section>

