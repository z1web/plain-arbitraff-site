<?php
/**
 * @var $this \src\Application
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=@$data['title']?></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>

    <meta name="keywords" content="<?=@$data['meta_keywords']?>" />
    <meta name="description" content="<?=@$data['meta_description']?>" />

    <link rel="shortcut icon" href="/web/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/web/style.css">

    <?php
    echo @$this->offersApi->getSiteData($this->siteId)['content_in_head'];
    ?>

    <style>

        .best-offer-widget {
            z-index: 98;
            position: fixed;
            bottom: 0px;
            left: 0;
            width: 100%;
        }

        .best-offer-widget__wrap:nth-child(1):hover {
            border: 2px solid #59af71;
        }

        .best-offer-widget__wrap {
            border: 2px solid rgba(255, 255, 255, 0);
            background-color: #fbfafa;
            border-radius: 10px;
            -webkit-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
            -moz-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
            box-shadow: 1px 1px 40px 0px rgba(50, 50, 50, 0.55);
            font-size: 20px;
            width: 420px;
            height: 95px;
            float: right;
            margin-bottom: 20px;
            margin-right: 20px;
            z-index: 9999;
            position: relative;
        }

        .best-offer-widget__close {
            cursor: pointer;
            position: absolute;
            top: 9px;
            font-size: 22px;
            color: #4b68b1;
            right: 9px;
            z-index: 99999;
        }

        .best-offer-widget__close-wrap {

        }

        .best-offer-widget__logo {
            display: inline-block;
            margin: 0 auto;
            margin-top: 17px;
            height: 49px;
            width: 110px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }

        .best-offer-widget__text {
            display: inline-block;
            padding-top: 20px;
            padding-left: 0;
            padding-right: 0px;
            color: #0a0a0a;
        }

        .best-offer-widget a {
            text-decoration: none !important;
        }

        @media screen and (max-width: 450px) {

            .best-offer-widget__logo {
                display: block;
                margin-top: 0;
            }

            .best-offer-widget__text {
                font-size: 19px;
                padding-top: 10px;
                text-align: center;
                margin-top: -4px;
                display: block;
            }

            .best-offer-widget__wrap {
                background-color: #fbfafa;
                /* border-radius: 10px; */
                -webkit-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
                -moz-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
                box-shadow: 1px 1px 40px 0px rgba(50, 50, 50, 0.55);
                font-size: 20px;
                width: 90%;
                height: 120px;
                float: inherit;
                margin-bottom: 0px;
                margin-right: 0px;
                margin: 0 auto;
                text-align: center;
            }
        }
    </style>

    <script>
        bestOfferWidgetClosed = false;
        window.onscroll = function () {
            if (!bestOfferWidgetClosed)
                $('.best-offer-widget').slideDown();
        }

        function closeBest() {
            $('.best-offer-widget').slideUp();
            bestOfferWidgetClosed = true;
        }
    </script>
</head>
<body>
<?php
echo @$this->offersApi->getSiteData($this->siteId)['content_body'];
?>
<body class="wrap">
    <header class="header">
        <div class="header__topline">
            <div class="container">
                <br><br>
                <a href="/"><img src="/web/logo.png" width="190" style="float: left"></a>

                <img src="/web/odobrim.png" width="150" style="float: right">
            </div>
        </div>
        <div class="header__bottomline">
            <div class="container">
                <h3 class="header__title" style="text-align: center">Мы рекомендуем заполнить максимальное количество заявок — это увеличит ваши шансы на получение займа до 100%</h3><br>
            </div>
        </div>
    </header>
    <div class="container">

        <?php
        require_once $data['view'].'.php';
        ?>

        <?php require_once 'footer-section.php'; ?>
    </div>

</body>
</html>