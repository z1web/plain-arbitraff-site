<?php
/**
 * @var $this \src\Application
 */
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=@$data['title']?></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>

    <meta name="keywords" content="<?=@$data['meta_keywords']?>" />
    <meta name="description" content="<?=@$data['meta_description']?>" />
    <meta name="google-site-verification" content="zTnC-nKIwY482SGozW70Vg3A0SNvTAvnuTq0rJpOVFI" />
    <link rel="apple-touch-icon" sizes="180x180" href="/web/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/web/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/web/favicon-16x16.png">

    <link rel="stylesheet" href="/web/style.css?v=3">

    <?php
    echo @$this->offersApi->getSiteData($this->siteId)['content_in_head'];
    ?>
</head>
<body>

<body class="wrap">
    <header class="header">
        <div class="header__topline">
            <div class="container">
                <br><br>
                <a href="/"><img src="/web/logo.png" width="160" style="float: left"></a>

                <img src="/web/odobrim.png" width="150" style="float: right">
            </div>
        </div>
        <div class="header__bottomline">
            <div class="container">
                <h3 class="header__title" style="text-align: center">Мы рекомендуем заполнить максимальное количество заявок — это увеличит ваши шансы на получение займа до 100%</h3><br>
            </div>
        </div>
    </header>
    <div class="container">

        <?php
        require_once $data['view'].'.php';
        ?>

        <?php require_once 'footer-section.php'; ?>
    </div>

</body>
</html>