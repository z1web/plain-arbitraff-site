<?php

/**
 * @var $this \src\Application
 */


$affId = 2222;

?>

<section class="offers">
    <?php


    foreach ($data['offers'] as $offer): ?>

        <a class="bem_offer bborder6" target="_blank" href="<?=$this::buildLink($offer,$affId);?>" >
            <span class="bem_offer__note"><?=$offer['opt_prolong']?></span>
            <div class="bem_offer__image">
                <div class="offer-logo" style="background-image: url('<?=$this::buildLogoLink($offer['logoPath']);?>');"></div>
            </div>

            <div class="bem_offer__info">
                <div class="bem_param bem_param_type_coin"><strong>до <?= $offer['maxcreditsum'] ?> руб.</strong></div>
                <div class="bem_param bem_param_type_percent">от&nbsp<span><?= $offer['mincreditpercent'] ?>%</span></div>
                <div class="bem_param ">Срок: 62-365 дней<br></div>
            </div>

            <div class="bem_offer__text">
                <strong><?= $offer['opt_history'] ?></strong>
                <br>
                <?= $offer['opt_early'] ?>
            </div>

            <div class="bem_offer__get">
                <div class="button" >
                    Получить деньги
                </div>
            </div>
        </a>

    <?php endforeach; ?>

</section>
