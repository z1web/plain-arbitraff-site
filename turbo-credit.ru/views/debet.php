<?php
require_once __DIR__ . '/../../autoload.php';

$offers = new \src\OffersAPI(new \src\FileCache());
$offers = $offers->getOffers(80);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=@$data['title']?></title>
    <meta name="keywords" content="<?=@$data['meta_keywords']?>" />
    <meta name="description" content="<?=@$data['meta_description']?>" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>


    <link rel="stylesheet" href="/web/bootstrap.css">
    <link rel="stylesheet" href="/web/style.css?v=2">

    <link rel="apple-touch-icon" sizes="180x180" href="/web/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/web/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/web/favicon-16x16.png">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178758640-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-178758640-1');
    </script>


    <script
            src="https://www.turbo-credit.ru/web/gyc.js"
    ></script>

    <script>
        $(document).ready(function() {
            $('.offer').matchHeight();
            var gastr = new GaSrt('UA-178758640-1', 'a', 'aff_sub5');
            gastr.go();
        });
    </script>
    <style>
        * {
            font-family: 'Open Sans Condensed', sans-serif!important;
        }

        body {
            background: linear-gradient(to bottom, #376f96, #15b4b9);
        }
        header {
            padding: 20px;
            color: #fff!important;
        }

        .offers {
            padding: 20px;
        }

        .inner_offer {
            padding: 10px;
            background: #fff;
            text-align: center;
        }

        .offer {
            background: linear-gradient(to bottom, #376f96, #15b4b9);
            /*width: 300px;*/
            padding: 10px;
            /*margin: 10px;*/
            font-size: 20px;
        }

        .offer span {
            color: #000!important;
        }

        .offers a:hover {
            text-decoration: none!important;
        }

        .button{
            display:inline-block;
            padding:0.46em 1.6em;
            border:0.1em solid #000000;
            margin:0 0.2em 0.2em 0;
            border-radius:0.12em;
            box-sizing: border-box;
            text-decoration:none;
            font-family:'Roboto',sans-serif;
            font-weight:300;
            color:#000000;
            text-shadow: 0 0.04em 0.04em rgba(0,0,0,0.35);
            background-color:#FFFFFF;
            text-align:center;
            transition: all 0.15s;
        }
        .button:hover{
            text-shadow: 0 0 2em rgba(255,255,255,1);color:#00a0d5;border-color:#00a0d5;

        }
        @media all and (max-width:30em){
             .button{
                display:block;
                margin:0.4em auto;
                 }
        }

        .offer__logo {
            width: 100%;
            height: 130px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
        }

        .menu {
            float: right;
            color: #367599;
            padding: 7px;
            font-size: 22px;
            background-color: #fff;
            margin: 2px;
        }
    </style>
</head>

<body>


<div>
    <header class="container">
        <div >
            <div class="col-xs-12"><h3>ДЕБЕТОВЫЕ КАРТЫ</h3>


                <br style="clear: both">
                <h4>В данном разделе представлены лучшие дебетовые карты России</h4>
            </div>

        </div>
    </header>


    <div class="offers container">

        <?php
        function buildLink($offer) {
            return 'https://generiq.go2cloud.org/aff_c?offer_id='.$offer['generiq_offer_id'].'&aff_id=1007';
        }

        function buildLogoLink($link) {
            return str_replace('//arbitraff.ru/', '', $link);
        }

        foreach ($offers['offers'] as $offer) {

            ?>
            <div class="offer col-md-4 col-sm-6 col-xs-12">
                <a href="<?=buildLink($offer)?>" target="_blank">
                    <div class="inner_offer">
                        <h3><?=$offer['name']?></h3><br>
                        <div class="offer__logo" style="background-image: url('<?=buildLogoLink($offer['logoPath']);?>')"></div>
                        <br>
                        <?=$offer['descriptionfull']?>
                        <br>
                        <a href="<?=buildLink($offer)?>" target="_blank" class="button">Получить</a>
                    </div>
                </a>
            </div>

            <?php
        }
        ?>

    </div>

    <br><br>


    <footer style="padding:20px;text-align: center;font-size: 15px;background-color: #dedede">Copyright 2020<br>Содержание сайта не является рекомендацией или офертой и носит информационно-справочный характер.</footer>
</body>
</html>