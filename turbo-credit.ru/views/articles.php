<?php
if (isset($_GET['id'])) {

    foreach ($data['articles']['result_data'] as $article) {
        if ((int)$article['id'] === (int)$_GET['id']) {
            ?>
            <div>
                <h3 class="article__title"><?= $article['name'] ?></h3>
                <div class="article__description">
                    <?= $article['text'] ?>
                </div>
                <small><?= $article['pub_date'] ?></small>
            </div>
            <?php
        }
    }


} else {


    foreach ($data['articles']['result_data'] as $article) { ?>
        <div>
            <a href="/articles?id=<?= $article['id'] ?>">
                <h3 class="article__title"><?= $article['name'] ?></h3>
            </a>
            <div class="article__description">
                <?= $article['description'] ?>
            </div>
            <small><?= $article['pub_date'] ?></small>
        </div>
        <?php
    }
}