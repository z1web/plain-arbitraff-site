<?php
/**
 * @var $data array
 */


?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=@$data['title']?></title>
    <meta name="keywords" content="<?=@$data['meta_keywords']?>" />
    <meta name="description" content="<?=@$data['meta_description']?>" />
    <meta name="google-site-verification" content="m8w-86Kh78wjnEEEa6BT5KzPm4mQ3jsPCsotFGs2t-M" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>

    <link rel="stylesheet" href="/web/style.css?v=4">

    <link href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAANzc3ADIyMgBwcHAANDQ0AP7+/gAvLy8AbW1tADExMQAzMzMAcXFxAO3t7QAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAApmZmZmZkAAJmZmZmZmXAAmZmZmZmZMACZmZmZmSmgAJmZmZmSVTAAmZmZmZmJoACZmZmZmZmgAJmZmZmZmaAAmZmZmZmZAACbu7u7u7AAAAGJmZmZkAAAAAAABGIgAAAAAAAAAAAAAAAAAAAAAAD//wAA//8AAMADAADAAQAAwAEAAMABAADAAQAAwAEAAMABAADAAQAAwAMAAMAHAADgBwAA/4cAAP//AAD//wAA" rel="icon" type="image/x-icon" />

    <?php
    $head = file_get_contents('http://arbitraff.ru/api/get-site-data/?siteId=75&hash=hjsadSdsvBVdCsdsdsbvg');
    $head = json_decode($head,1);
    echo @$head['content_in_head'];
    ?>
</head>

<body>


<header>
    <div class="container">
        <div class="logo">
            <p class="title">zaim-srochno.com</p>
            <p class="subtitle">Деньги в кредит с 99% одобрением.<br>Только проверенные компании</p>
        </div>
        <div class="contact">

        </div>

    </div>
</header>

<?php
require_once $data['view'].'.php';
?>


<?php require_once 'footer-section.php'; ?>

</body>
</html>