<?php
/**
 * site renderer
 *
 * @author D.Kuschenko <kd@linkprofit.com>
 */

require_once __DIR__ . '/../../autoload.php';

$offers = new \src\OffersAPI(new \src\FileCache());
$data = $offers->getOffers(77);


?>


<?php
/**
 * @var $data array
 */
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Займы в Казахстане</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>

    <link rel="stylesheet" href="/web/style_kz.css">

    <link rel="apple-touch-icon" sizes="180x180" href="/web/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/web/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/web/favicon-16x16.png">


    <?php
    echo @$offers->getSiteData(77)['content_in_head'];
    ?>

    <style>
        .menu {
            padding: 6px;
            margin: 5px;
            background-color: #e8ac51;
            float: right;
            color: #3a3a3a;
        }
    </style>



    <style>

        .best-offer-widget {
            z-index: 98;
            position: fixed;
            bottom: 0px;
            left: 0;
            width: 100%;
        }

        .best-offer-widget__wrap:nth-child(1):hover {
            border: 2px solid #59af71;
        }

        .best-offer-widget__wrap {
            border: 2px solid rgba(255, 255, 255, 0);
            background-color: #fbfafa;
            border-radius: 10px;
            -webkit-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
            -moz-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
            box-shadow: 1px 1px 40px 0px rgba(50, 50, 50, 0.55);
            font-size: 20px;
            width: 420px;
            height: 95px;
            float: right;
            margin-bottom: 20px;
            margin-right: 20px;
            z-index: 9999;
            position: relative;
        }

        .best-offer-widget__close {
            cursor: pointer;
            position: absolute;
            top: 9px;
            font-size: 22px;
            color: #4b68b1;
            right: 9px;
            z-index: 99999;
        }

        .best-offer-widget__close-wrap {

        }

        .best-offer-widget__logo {
            display: inline-block;
            margin: 0 auto;
            margin-top: 17px;
            height: 49px;
            width: 110px;
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center;
        }

        .best-offer-widget__text {
            display: inline-block;
            padding-top: 20px;
            padding-left: 0;
            padding-right: 0px;
            color: #0a0a0a;
        }

        .best-offer-widget a {
            text-decoration: none !important;
        }

        @media screen and (max-width: 450px) {

            .best-offer-widget__logo {
                display: block;
                margin-top: 0;
            }

            .best-offer-widget__text {
                font-size: 19px;
                padding-top: 10px;
                text-align: center;
                margin-top: -4px;
                display: block;
            }

            .best-offer-widget__wrap {
                background-color: #fbfafa;
                /* border-radius: 10px; */
                -webkit-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
                -moz-box-shadow: 1px 1px 10px 0px rgba(50, 50, 50, 0.75);
                box-shadow: 1px 1px 40px 0px rgba(50, 50, 50, 0.55);
                font-size: 20px;
                width: 90%;
                height: 120px;
                float: inherit;
                margin-bottom: 0px;
                margin-right: 0px;
                margin: 0 auto;
                text-align: center;
            }
        }
    </style>

    <script>
        bestOfferWidgetClosed = false;
        window.onscroll = function () {
            if (!bestOfferWidgetClosed)
                $('.best-offer-widget').slideDown();
        }

        function closeBest() {
            $('.best-offer-widget').slideUp();
            bestOfferWidgetClosed = true;
        }
    </script>



</head>

<body>
<?php
echo @$offers->getSiteData(77)['content_body'];
?>

<div>
    <header class="container">
        <div class="row logo-row">
            <div class="col-xs-12">
                <div class="logo"
                     style="background-image: url('/web/logo2.png');width:220px;height:60px;background-size: contain;background-repeat: no-repeat;float: left;margin-right: 20px;"></div>
                <h1 class="logo-header-text">Быстрые онлайн-займы на карту в Казахстане</h1>
                <br>

            </div>

        </div>
    </header>

    <?php
    require_once 'offers-section.php';
    ?>
    <br><br>

</body>
</html>
