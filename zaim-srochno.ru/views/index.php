<?php
/**
 * @var $data array
 */
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=@$data['title']?></title>
    <meta name="keywords" content="<?=@$data['meta_keywords']?>" />
    <meta name="description" content="<?=@$data['meta_description']?>" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous"></script>
    <script
            src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>


    <link rel="stylesheet" href="/web/bootstrap.css">
    <link rel="stylesheet" href="/web/style.css?v=4">

    <link rel="apple-touch-icon" sizes="180x180" href="/web/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/web/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/web/favicon-16x16.png">

    <?php
    $head = file_get_contents('http://arbitraff.ru/api/get-site-data/?siteId=74&hash=hjsadSdsvBVdCsdsdsbvg');
    $head = json_decode($head,1);
    echo @$head['content_in_head'];
    ?>

    <style>
        .menu {
            padding: 6px;
            margin: 5px;
            background-color: #e8ac51;
            float: right;
            color: #3a3a3a;
        }
    </style>
</head>

<body>


<div>
    <header class="container">
        <div class="row logo-row">
            <div class="col-xs-12">
                <div class="logo"
                     style="background-image: url('/web/logo.png');width:220px;height:60px;background-size: contain;background-repeat: no-repeat;float: left;margin-right: 20px;"></div>
                <h1 class="logo-header-text">Быстрые онлайн-займы на карту в России</h1>
                <br>
                <a class="menu" href="/">Займы</a>
                <a class="menu" href="/debet">Дебетовые карты</a>
                <a class="menu" href="/articles">Статьи</a>
            </div>

        </div>
    </header>

    <?php
    require_once $data['view'].'.php';
    ?>
    <br><br>

</body>
</html>