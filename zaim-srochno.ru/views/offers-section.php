<div class="main container">
    <div class="row">



        <div class="clearfix"></div>
        <div class="col-md-12">
            <div class="headerTextBlock">
                <h3 class="text-center">Заполнение двух и более заявок гарантирует получение требуемой суммы</h3>

            </div>
        </div>

        <div class="row header-row table-content-row hidden-xs">
            <div class="table-col-1 first">
                Компания
            </div>
            <div class="table-col-1">
                Процентная ставка
            </div>
            <div class="table-col-2">
                сумма
            </div>
            <div class="table-col-2 summ-col">
                срок
            </div>
            <div class="table-col-3">
                доп. информация
            </div>
        </div>
    </div>
</div>

<div class="propositions container">

    <?php
    function buildLink($offer) {
        return 'https://generiq.go2cloud.org/aff_c?offer_id='.$offer['generiq_offer_id'].'&aff_id=1007';
    }
    function buildLogoLink($link) {
        return str_replace('//arbitraff.ru/', '', $link);
    }
    foreach ($data['offers'] as $offer): ?>


        <a class="row table-content-row top js-go"
           href="<?=buildLink($offer)?>"
           data-event="oursite" target="_blank">
            <div class="table-col-1 first">
                <div class="desc">
                    <span class="img-submit">
                        <img class="img-responsive" src="<?=buildLogoLink($offer['logoPath']);?>">
                    </span>
                </div>
            </div>
            <div class="table-col-1">
                <div class="title"> Ставка</div>
                <div class="desc"> от&nbsp<span><?= $offer['mincreditpercent'] ?>% в день</div>
            </div>
            <div class="table-col-2 summ-col">
                <div class="title"> сумма</div>
                <div class="desc">до <?= $offer['maxcreditsum'] ?> руб.
                </div>
            </div>

            <div class="table-col-2">
                <div class="title"> срок</div>
                <div class="desc">
                    от <?= $offer['mincreditterm'] ?>
                    до <?= $offer['maxcreditterm'] ?> дней
                </div>
            </div>
            <div class="table-col-3">
                <div class="title"> дополнительная информация</div>
                <div class="desc">
                    <?= $offer['opt_history'] ?><br>
                    <?= $offer['opt_early'] ?>

                </div>
            </div>
            <div class="table-col-1">
                <div class="table-btn-row"><span class="btn-submit">ПОДАТЬ ЗАЯВКУ</span></div>
            </div>
        </a>



    <?php endforeach; ?>

</div>

<?php require_once 'footer-section.php'; ?>