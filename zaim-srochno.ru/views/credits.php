<?php
/**
 * @var $data array
 */
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=@$data['title']?></title>
    <meta name="keywords" content="<?=@$data['meta_keywords']?>" />
    <meta name="description" content="<?=@$data['meta_description']?>" />
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js"
    ></script>


    <link rel="stylesheet" href="/web/bootstrap.css">
    <link rel="stylesheet" href="/web/style.css?v=2">

    <link rel="apple-touch-icon" sizes="180x180" href="/web/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/web/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/web/favicon-16x16.png">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@300&display=swap" rel="stylesheet">
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178755161-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-178755161-1');
    </script>

    <script>
        $(document).ready(function () {
            $('.offer').matchHeight();
        })
    </script>

    <script
        src="http://zaim-srochno.ru/web/gyc.js"
    ></script>

    <script>
        $(document).ready(function() {
            var gastr = new GaSrt('UA-178755161-1', 'a', 'aff_sub5');
            gastr.go();
        });
    </script>
    <style>
        * {
            font-family: 'Open Sans Condensed', sans-serif!important;
        }

        body {
            background: linear-gradient(to bottom, #376f96, #15b4b9);
        }
        header {
            padding: 20px;
            color: #fff!important;
        }

        .offers {
            padding: 20px;
        }

        .inner_offer {
            padding: 10px;
            background: #fff;
            text-align: center;
        }

        .offer {
            background: linear-gradient(to bottom, #376f96, #15b4b9);
            /*width: 300px;*/
            padding: 10px;
            /*margin: 10px;*/
            font-size: 20px;
        }

        .offer span {
            color: #000!important;
        }

        .offers a:hover {
            text-decoration: none!important;
        }

        .button{
            display:inline-block;
            padding:0.46em 1.6em;
            border:0.1em solid #000000;
            margin:0 0.2em 0.2em 0;
            border-radius:0.12em;
            box-sizing: border-box;
            text-decoration:none;
            font-family:'Roboto',sans-serif;
            font-weight:300;
            color:#000000;
            text-shadow: 0 0.04em 0.04em rgba(0,0,0,0.35);
            background-color:#FFFFFF;
            text-align:center;
            transition: all 0.15s;
        }
        .button:hover{
            text-shadow: 0 0 2em rgba(255,255,255,1);color:#00a0d5;border-color:#00a0d5;

        }
        @media all and (max-width:30em){
             .button{
                display:block;
                margin:0.4em auto;
                 }
        }

        .offer__logo {
            width: 100%;
            height: 130px;
            background-size: contain;
            background-position: center;
            background-repeat: no-repeat;
        }

        .menu {
            float: right;
            color: #367599;
            padding: 7px;
            font-size: 22px;
            background-color: #fff;
            margin: 2px;
        }
    </style>
</head>

<body>


<div>
    <header class="container">
        <div >
            <div class="col-xs-12"><h3>ПОТРЕБИТЕЛЬСКИЕ КРЕДИТЫ</h3>


                <br style="clear: both">
                <h4>В данном разделе представлены лучшие кредиты России</h4>
            </div>

        </div>
    </header>

    <script>
        $(document).ready(function () {
            params  = new URLSearchParams(window.location.search);
            if (params.get('id')) {
                console.log(params.get('id'));
                let findStr = ".offer[offerId='" + params.get('id') + "']";
                $('.main_offer').html($(findStr).html());
                $(findStr).hide();
            }
        })
    </script>

    <div class="offers container">
        <div class="offer main_offer col-md-12 col-sm-12 col-xs-12" >

        </div>


        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="61">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=61&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Почта Банк</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/pochta.jpg')"></div>
                    <br>
                    <b>Сумма: </b><span> до 4 000 000 руб.</span><br>
                    <b>Возраст: </b><span> от 18 лет </span><br>
                    <b>Ставка: </b><span> от 6,9%</span><br>
                    <b>Срок кредита : </b><span> от 36 мес. до 5 лет</span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=61&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 650</small>
                </div>
            </a>
        </div>

        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="72">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=72&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Банк Восточный</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/vost.jpg')"></div>
                    <br>
                    <b>Сумма: </b><span> от 25 000 до 1 500 000 руб.</span><br>
                    <b>Возраст: </b><span> от 21 до 75 лет </span><br>
                    <b>Ставка: </b><span> 9% годовых</span><br>
                    <b>Срок кредита : </b><span> 12-60 месяцев </span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=72&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 1460</small>
                </div>
            </a>
        </div>


        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="69">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=69&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Совкомбанк</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/sovk.png')"></div>
                    <br>
                    <b>Сумма: </b><span> до 1 000 000 руб.</span><br>
                    <b>Возраст: </b><span> от 20 до 67 лет </span><br>
                    <b>Ставка: </b><span> от 8,9%</span><br>
                    <b>Срок кредита : </b><span> до 5 лет </span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=69&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 963</small>
                </div>
            </a>
        </div>

        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="6">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=6&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>СКБ-банк </h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/skb.png')"></div>
                    <br>
                    <b>Сумма: </b><span> до 1 500 000 рублей</span><br>
                    <b>Возраст: </b><span> от 23 до 70 лет </span><br>
                    <b>Ставка: </b><span> от 7%</span><br>
                    <b>Срок кредита : </b><span> до 5 лет</span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=6&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 705</small>
                </div>
            </a>
        </div>


        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="49">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=49&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>ВТБ</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/vtb.jpg')"></div>
                    <br>
                    <b>Сумма: </b><span> от 50 000 до 5 000 000 руб.</span><br>
                    <b>Возраст: </b><span> с 21 года </span><br>
                    <b>Ставка: </b><span> от 6,4%</span><br>
                    <b>Срок кредита : </b><span> до 7 лет</span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=49&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 1000</small>
                </div>
            </a>
        </div>

        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="99">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=99&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Райффайзен Банк</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/raif.jpg')"></div>
                    <br>
                    <b>Сумма: </b><span> до 2 000 000 руб.</span><br>
                    <b>Возраст: </b><span> от 23 до 67 лет </span><br>
                    <b>Ставка: </b><span> от 7,99%</span><br>
                    <b>Срок кредита : </b><span> до 5 лет </span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=99&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 3292</small>
                </div>
            </a>
        </div>



        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="75">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=75&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Tinkoff Bank</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/tinkoff.png')"></div>
                    <br>
                    <b>Сумма: </b><span> до 1 000 000 руб.</span><br>
                    <b>Возраст: </b><span> от 18 до 70 лет </span><br>
                    <b>Ставка: </b><span> от 12% </span><br>
                    <b>Срок кредита : </b><span> от 1 до 3 лет </span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=77&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 2673</small>
                </div>
            </a>
        </div>


        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="74">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=74&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>"Газпромбанк"</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/gaz.png')"></div>
                    <br>
                    <b>Сумма: </b><span> до 5 000 000 руб.</span><br>
                    <b>Возраст: </b><span> от 20 до 70 лет </span><br>
                    <b>Ставка: </b><span> от 6.9% </span><br>
                    <b>Срок кредита : </b><span> от 13 до 84 месяцев </span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=77&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 354</small>
                </div>
            </a>
        </div>

        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="73">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=73&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Открытие банк</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/otk.svg')"></div>
                    <br>
                    <b>Сумма: </b><span> до 5 000 000 руб.</span><br>
                    <b>Возраст: </b><span> от 21 до 68 лет </span><br>
                    <b>Ставка: </b><span> от 6.9% </span><br>
                    <b>Срок кредита : </b><span> до 5 лет </span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=73&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 2209</small>
                </div>
            </a>
        </div>



        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="71">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=71&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>ПАО КБ «УБРиР»</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/ur.png')"></div>
                    <br>
                    <b>Сумма: </b><span> до 5 000 000 руб.</span><br>
                    <b>Возраст: </b><span> от 19 лет </span><br>
                    <b>Ставка: </b><span> от 6,5%</span><br>
                    <b>Срок кредита : </b><span> 3, 5, 7, 10 лет </span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=71&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 429</small>
                </div>
            </a>
        </div>




        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="2">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=2&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Альфа-Банк</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/alfa.png')"></div>
                    <br>
                    <b>Сумма: </b><span> до 5 000 000 рублей</span><br>
                    <b>Возраст: </b><span> с 21 года </span><br>
                    <b>Ставка: </b><span> от 6,5 %</span><br>
                    <b>Срок кредита : </b><span> от 1 до 7 лет</span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=2&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 1326</small>
                </div>
            </a>
        </div>

        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="68">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=68&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Банк Пойдем</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/poidem.png')"></div>
                    <br>
                    <b>Сумма: </b><span> от 50 000 до 500 000 рублей</span><br>
                    <b>Возраст: </b><span> с 22 года </span><br>
                    <b>Ставка: </b><span> от 5,5 %</span><br>
                    <b>Срок кредита : </b><span> до 13 месяцев</span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=68&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 2534</small>
                </div>
            </a>
        </div>
        <div class="offer col-md-4 col-sm-6 col-xs-12" offerId="112">
            <a href="https://generiq.go2cloud.org/aff_c?offer_id=112&aff_id=1007" target="_blank">
                <div class="inner_offer">
                    <h3>Ак Барс</h3><br>
                    <div class="offer__logo" style="background-image: url('/web/logos/credits/ak.png')"></div>
                    <br>
                    <b>Сумма: </b><span> до 400 0000 рублей</span><br>
                    <b>Возраст: </b><span> с 21 года </span><br>
                    <b>Ставка: </b><span> от 7,7 %</span><br>
                    <b>Срок кредита : </b><span> до 7 лет</span><br>
                    <a href="https://generiq.go2cloud.org/aff_c?offer_id=112&aff_id=1007" target="_blank" class="button">Получить</a>
                    <br><small>Лицензия № 2590</small>
                </div>
            </a>
        </div>

    </div>

    <br><br>


    <footer style="padding:20px;text-align: center;font-size: 15px;background-color: #dedede">Copyright 2020<br>Содержание сайта не является рекомендацией или офертой и носит информационно-справочный характер.</footer>
</body>
</html>