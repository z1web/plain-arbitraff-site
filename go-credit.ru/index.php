<?php
/**
 * @author D.Kuschenko <kd@linkprofit.com>
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 1);

require_once __DIR__ . '/../src/OffersCacher.php';
require_once __DIR__ . '/../src/OffersAPI.php';

$siteId = 69;
$offersApi = new \src\OffersAPI();

$route = parse_url($_SERVER['REQUEST_URI'])['path'];
if ($route === '/clean-cache') {
    $offersApi->cache->clearAllCache();
    die;
}

$offers = $offersApi->getOffers($siteId);

require_once __DIR__ . '/template/main.php';