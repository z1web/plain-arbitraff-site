<div class="offers container">
<!--    <h2 class="offers__title">Оформить кредит онлайн на банковскую карту в России за 15 минут:</h2>-->
    <div class="row">

        <?php foreach ($offers['offers'] as $offer): ?>
        <?php
        $link =$offer['link'];
        ?>
        <div class="col-md-12">


            <div class="offer" link="<?= $link ?>">
                <div class="offer__top">

                    <div class="offer__logo-wrap">
                        <a href="<?= $link ?>" target="_blank">
                            <img src="<?= $offer['logoPath'] ?>" class="img-responsive offer__logo">
                        </a>
                    </div>

                    <div class="offer__desc-wrap">
                        <div class="offer__period-wrap">
                            <p class="offer__option-title">Сумма</p>
                            <p class="offer__option-desc"><?= $offer['builded_creditsum'] ?> руб.</p>
                        </div>

                        <div class="offer__period-wrap">
                            <p class="offer__option-title">Срок</p>
                            <p class="offer__option-desc">от <?= $offer['mincreditterm'] ?>
                                до <?= $offer['maxcreditterm'] ?> дней</p>
                        </div>
                        <div class="offer__period-wrap">
                            <p class="offer__option-title">Процент</p>
                            <p class="offer__option-desc"><?= $offer['builded_mincreditpercent'] ?></p>
                        </div>
                    </div>
                    <div class="offer__button-wrap">
                        <a id=""
                           href="<?= $link ?>"
                           target="_blank" class="offer__button offer__button__more">Подробнее</a>
                    </div>
                </div>
<!--                <div class="offer__bottom">-->
<!--                    <div class="offer__bottom-desc">-->
<!--                        <h4>-->
<!--                            <ul>-->
<!--                                <li>-->
<!--                                    --><?//= $offer['opt_early'] ?><!-- <br>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    --><?//= $offer['opt_history'] ?><!--<br>-->
<!--                                </li>-->
<!--                                <li>-->
<!--                                    --><?//= $offer['opt_prolong'] ?>
<!--                                </li>-->
<!--                            </ul>-->
<!--                        </h4>-->
<!--                    </div>-->
<!--                    <div class="offer__button-wrap">-->
<!--                        <a id=""-->
<!--                           href="--><?//= $link ?><!--"-->
<!--                           target="_blank" class="offer__button offer__button__get">Оформить</a>-->
<!--                    </div>-->
<!---->
<!--                </div>-->
            </div>

    </div>
        <?php endforeach; ?>
</div>