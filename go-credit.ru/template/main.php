<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Кредитный калькулятор / Go credit </title>

    <link rel="shortcut icon" href="/img/sites_favicons/69/favicon.ico"/>
</head>
<body>

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="template/style.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>


<div class="wrap">
    <header class="header">
        <div class="container">
            <a href="/offers"><img width="240" src="http://arbitraff.ru/img/sites_logos/2/1/8/5bead7cc6259f.png" alt=""
                                   class="img-responsive header__logo">
            <h1 class="header__title">Сервис подбора онлайн кредитов и займов в РФ</h1>
            </a>
        </div>
    </header>
    <div class="content">
        <div class="">

            <div class="condition">
                <div class="row">
                    <div class="col-md-4">
                        <div class="condition__item">
                            <img src="http://arbitraff.ru/templates/online-credits.ru/img/Paper-Plane-48.png" alt=""
                                 class="condition__logo">
                            <p class="condition__title">Отправьте заявки</p>
                            <div class="condition__desc">Для 100%-го одобрения, отправляйте заявку на кредит сразу в 2-3
                                организации.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="condition__item">
                            <img src="http://arbitraff.ru/templates/online-credits.ru/img/Clock-01-48.png" alt=""
                                 class="condition__logo">
                            <p class="condition__title">Дождитесь ответа</p>
                            <div class="condition__desc">Ответ об одобрении приходит в течении 5-15 минут по SMS, на
                                электронную почту или телефонным звонком.
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="condition__item">
                            <img src="http://arbitraff.ru/templates/online-credits.ru/img/Donate-48.png" alt=""
                                 class="condition__logo">
                            <p class="condition__title">Получите деньги</p>
                            <div class="condition__desc">Уже через 15-30 минут деньги поступят на банковскую карту
                                любого банка.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?require_once "offers-section.php";?>
        </div>
    </div>
    <div class="container">
        <p style="text-align:justify">
            Гражданство РФ <br>Возраст от 18 лет<br> Любая кредитная история
        <p>


        <div id="agreement" role="dialog" tabindex="-1" style="display: none" class="fade modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
                        <h2>Agreement head title</h2>
                    </div>
                    <div class="modal-body">Requirements for borrowers</div>
                </div>
            </div>
        </div>


        <a data-toggle="modal" data-target="#agreement">Agreement head title</a>


        </p>

        </p>


        <div id="privacy-policy" role="dialog" tabindex="-1" style="display: none" class="fade modal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" data-dismiss="modal" aria-hidden="true" class="close">x</button>
                        <h2>Privacy policy label</h2>
                    </div>
                    <div class="modal-body">Privacy policy</div>
                </div>
            </div>
        </div>


    </div>
    <footer class="footer">
        <div class="container">
            <ul class="nav">
                <li><a href="/offers">Выбрать кредит</a></li>
                <li><a href="/articles">Новости</a></li>
                <li><a href="/about">О нас</a></li>
                <li><a href="/contact">Контакты</a></li>
            </ul>
            <p>© 2018 go-credit.ru - кредиты онлайн на карту</p>
            <p>Сайт описывает подробную информацию об условиях кредитования, ведь получить кредит онлайн на карту без
                отказа, проверок, мгновенно в России можно за 15 минут не выходя из дома.</p>
        </div>
    </footer>
</div>

<script src="/js/data-icing.js?v=1556039759"></script>
</body>
</html>
